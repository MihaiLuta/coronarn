import React from 'react'
import { View, Alert, StyleSheet} from 'react-native'
import Toolbar from '../components/Toolbar'
import CustomButton from '../components/CustomButton'

import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'

import GeoLocation from '@react-native-community/geolocation'
import { GeoFire } from 'geofire'
import { Text } from 'native-base'

export default class CoronaScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = {tryAgain: false, msg: 'Checking if any person is exposed near you'}
        this.intervalId = -1
        this.uid = auth().currentUser.uid 
    }

    componentDidMount() {

        GeoLocation.watchPosition(position => {
            this.updateLocation(position.coords)    
        })

        this.getCurrentLocationEveryFiveSeconds()
    }

    getCurrentLocationEveryFiveSeconds = () => {
        this.intervalId = setInterval(() => {
            GeoLocation.getCurrentPosition(position =>  this.checkIfExposed(position.coords) )
        }, 5000)
    }

    updateLocation = (location) => {
        let ref = database().ref().child('user_locations')
        let geofire = new GeoFire(ref)

        geofire.set(this.uid, [location.latitude, location.longitude]).then(() => {
            console.log('added')
            database().ref().child('exposed').child(this.uid).child('lastUpdated').set(database.ServerValue.TIMESTAMP)
        }, (err) => {
            console.log(err)
        })

    }

    checkIfExposed = (location) => {

        let geofire = new GeoFire(database().ref().child('user_locations'))
        let query = geofire.query({
            center: [location.latitude, location.longitude],
            radius: 10
        })

        let keyEntered = query.on('key_entered', (key, location, distance) => {
            if (this.uid !== key) {
                keyEntered.cancel()
                window.clearInterval(this.intervalId)
                this.getExposedValue(key, data => {
                    if (data.lastUpdated === undefined) { this.getCurrentLocationEveryFiveSeconds() }
                    else if (data.exposed === 'yes' && this.getMinutes(data.lastUpdated) < 4) { this.exposedPersonFound() }
                    else { this.getCurrentLocationEveryFiveSeconds() }
                })
            }
        })
    }

    getExposedValue = (key, completion) => {
        database().ref().child('exposed').child(key).once('value').then(snap => {
            completion(snap.val())
        })
    }

    exposedPersonFound = () => {
        Alert.alert('Alert', 'There is some person exposed to covid, please move 20 feet away', [
            {text: 'OK', onPress: () => this.setState({msg: 'There is some person exposed to covid, please move 20 feet away, try again to see again', tryAgain: true})},
        ])
    }

    getMinutes = (timestamp) => {
        let today = new Date()
        let lastUpdated = new Date(timestamp)
        var diffMs = (today - lastUpdated)
        return diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000) // minutes
    }

    render() {
        return(
            <View style={{flex: 1, justifyContent: 'center'}}>
                <View style={{position: 'absolute', top: 0, width: '100%'}}>
                    <Toolbar title='Corona Nearby' onPress={() => this.props.navigation.openDrawer()}/>
                </View>

                <Text style={styles.centerText}>{this.state.msg}</Text>

                <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
                    {this.state.tryAgain ? <CustomButton title='Try Again' onPress={this.onTryAgainBtnTapped.bind(this)}/> : null}
                    <CustomButton title='Are you exposed to corona?' onPress={this.onExposedTapped.bind(this)}/>
                </View>
            </View>
        )
    }

    onExposedTapped = () => {
        const uid = auth().currentUser.uid

        Alert.alert('Corona', 'Are you exposed to corona?', [
            {text: 'Cancel'},
            {text: 'Yes', onPress: () => database().ref().child('exposed').child(uid).child('exposed').set('yes')},
            {text: 'No', onPress: () => database().ref().child('exposed').child(uid).child('exposed').set('no')}
          ])
    }

    onTryAgainBtnTapped = () => {
        this.setState({tryAgain: false, msg: 'Checking if any person is exposed near you'})
        this.getCurrentLocationEveryFiveSeconds()
    }

}

const styles = new StyleSheet.create({
    centerText: {
        textAlign: 'center',
        marginLeft: 16,
        marginRight: 16,
        fontFamily: 'Nunito-Bold',
        fontSize: 30,
        color: 'black'
    }
})
import React from 'react';
import { View, Text, StyleSheet, Image, Alert, TouchableOpacity, ActivityIndicator } from 'react-native';
import { firebase } from '@react-native-firebase/auth';
import StatsComponent from '../components/StatsComponent';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import StatsList from '../components/StatsList';

class HomeScreen extends React.Component {

  constructor(props) {
    super(props)

    this.state = { coronaData: {}, currentTime: new Date().toGMTString(), moreData: []}
  }


  componentDidMount() {
    const externalApiData = "https://api.apify.com/v2/key-value-stores/KUlj8EGfDGHiB0gU1/records/LATEST?disableRedirect=true";
    fetch(externalApiData).then(res => res.json()).then(obj => {
      this.setState({coronaData: obj})
      fetch(obj.historyData).then(res => res.json()).then(obj => {
        this.setState({moreData: obj.reverse().slice(0,20)})
      })
    })

    // setInterval(() => this.setState({currentTime: new Date().toGMTString()}), 1000)
  }

  numberFormat = (num) => {
    if (typeof(num) === 'undefined' || num === null || isNaN(null)) { return 'Loading ...' }
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,').replace(".00", "")
 }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.totalCaseView}>
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Image source={require('../assets/drawer.png')} style={{width: 30, height: 30}}/>
          </TouchableOpacity>

          <Text style={styles.dateTextStyle}>{this.state.currentTime}</Text>
          <Text style={styles.coronaVirusTxt}>Corona Virus Cases</Text>
          <Text style={{...styles.coronaVirusTxt, fontSize: 40, fontFamily: 'Nunito-ExtraBold',}}>{this.numberFormat(this.state.coronaData.infected + this.state.coronaData.tested + this.state.coronaData.recovered + this.state.coronaData.deceased)}</Text>
          <Text style={styles.dateTextStyle}>{this.state.coronaData.country}</Text>
        </View>

        <View style={{flexDirection: 'row', marginLeft: '5%', marginRight: '5%', marginTop: '-5%', justifyContent: 'space-between'}}>
          <StatsComponent title='DEATHS' value={this.state.coronaData.deceased} color={'rgb(255,59,48)'}/>
          <StatsComponent title='RECOVERED' value={this.state.coronaData.recovered} color={'rgb(52,199,89)'}/>
        </View>

        <View style={{flexDirection: 'row', margin: '5%', justifyContent: 'space-between'}}>
          <StatsComponent title='INFECTED' value={this.state.coronaData.infected} color={'rgb(90,200,250)'}/>
          <StatsComponent title='TESTED' value={this.state.coronaData.tested} color={'rgb(255,149,0)'}/>
        </View>
        <Text style={{marginLeft: '5%', fontFamily: 'Nunito-Regular'}}>More History Info</Text>
        <ActivityIndicator animating={this.state.moreData.length === 0}/>
        <FlatList 
          data={this.state.moreData}
          keyExtractor={(_, index) => index.toString()}
          renderItem={({ item}) => <StatsList data={item}/> }
        />
      </ScrollView>
    )
  };
}

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  totalCaseView: {
    height: 250,
    width: '100%',
    backgroundColor: 'rgb(221, 90, 83)',
    padding: 10
  },

  dateTextStyle: {
    fontFamily: 'Nunito-Regular',
    fontSize: 15,
    fontWeight: '600',
    color: 'white',
    margin: 10
  },

  coronaVirusTxt: {
    fontFamily: 'Nunito-Bold',
    fontSize: 25,
    color: 'white',
    marginLeft: 10
  }
})
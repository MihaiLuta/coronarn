import React from 'react'
import { View, Modal, ActivityIndicator, Alert, FlatList} from 'react-native'
import Toolbar from '../components/Toolbar'
import NewsComponent from '../components/NewsComponent'
import WebViewScreen from './WebViewScreen'


export default class NewsScreen extends React.Component {

    constructor(props) {
        super(props)

        this.state = {news: [], articleUrl: null}
    }

    componentDidMount() {
        // const url = 'http://newsapi.org/v2/top-headlines?country=ro&category=health&apiKey=fe8159b0aa95461fbef3b9ada05ded6a'
        // const url = 'https://newsapi.org/v2/everything?q=corona&apiKey=fe8159b0aa95461fbef3b9ada05ded6a'
        const url = 'https://newsapi.org/v2/top-headlines?q=coronavirus&apiKey=fe8159b0aa95461fbef3b9ada05ded6a&country=ro'
        fetch(url).then(res => res.json()).then(rsl => {
            this.setState({news: rsl.articles})
        }).catch(err => Alert.alert(err.message))
    }

    render() {
        return(
            <View style={{flex: 1}}>
                <Toolbar title='News' onPress={() => this.props.navigation.openDrawer()}/>
                <ActivityIndicator animating={this.state.news.length === 0}/>
                <FlatList 
                    data={this.state.news}
                    keyExtractor={(_, index) => index.toString()}
                    renderItem={({ item }) => <NewsComponent news={item} onPress={() => this.onNewsTapped(item)}/>}
                />

                <Modal animationType='slide' visible={this.state.articleUrl !== null}>
                    <WebViewScreen url={this.state.articleUrl} backPress={() => this.setState({articleUrl: null})}/>
                </Modal>

            </View>
        )
    }

    onNewsTapped = (article) => this.setState({articleUrl: article.url})

}
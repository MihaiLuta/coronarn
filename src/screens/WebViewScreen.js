import React from 'react'
import { TouchableOpacity, Image, SafeAreaView, Text, ActivityIndicator} from 'react-native'
import { WebView } from 'react-native-webview'

export default class WebViewScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {loading: true}
    }

    render() {
        return(
        <SafeAreaView style={{flex: 1}}>
            <TouchableOpacity onPress={this.props.backPress} style={{margin: 5}}>
                <Image source={require('../assets/back.png')} style={{width: 32, height: 32, tintColor: 'black'}} />
            </TouchableOpacity>
            <WebView
                javaScriptEnabled={true}
                domStorageEnabled={true}
                onLoad={() => this.setState({loading: false})}
                source={{uri: this.props.url}} />
            
            {
                this.state.loading && (
                    <ActivityIndicator 
                        style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0}}
                        size='large'
                    />
                )
            }

        </SafeAreaView>
        )
    }
}
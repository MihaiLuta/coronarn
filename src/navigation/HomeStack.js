import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'

import HomeScreen from '../screens/HomeScreen'
import NewsScreen from '../screens/NewsScreen'
import CoronaScreen from '../screens/CoronaScreen'
import DrawerHeader from '../components/DrawerHeader'


const Drawer = createDrawerNavigator()

export default function HomeStack() {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerHeader {...props} />}>
      <Drawer.Screen name='Home' component={HomeScreen} options={{headerShown: false}}/>
      <Drawer.Screen name='News' component={NewsScreen} />
      <Drawer.Screen name='Corona Nearby' component={CoronaScreen} />
    </Drawer.Navigator>
  );
}
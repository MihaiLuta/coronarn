import React, { createContext, useState } from 'react';
import auth, { firebase } from '@react-native-firebase/auth';

export const AuthContext = createContext({});
export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    return (
      <AuthContext.Provider
        value={{
          user,
          setUser,
          login: async (email, password) => {
            try {
              await auth().signInWithEmailAndPassword(email, password);
            } catch (e) {
              console.log(e);
            }
          },
          register: async (email, password, displayName) => {
            try {
              await auth().createUserWithEmailAndPassword(email, password)
              .then((res) => res.user.updateProfile({displayName : displayName}))
              .then(() => auth().currentUser.reload)
          // .then((res) => console.log(res) );
            } catch (e) {
              console.log(e);
            }
          },
          logout: async () => {
            try {
              await auth().signOut();
            } catch (e) {
              console.error(e);
            }
          }
        }}
      >
        {children}
      </AuthContext.Provider>
    );
  };
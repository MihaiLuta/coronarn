import React from 'react'
import { TouchableOpacity, Image, Text, StyleSheet } from 'react-native'

export default class NewsComponent extends React.Component {
    render() {
        return(
            <TouchableOpacity style={styles.contentStyle} onPress={this.props.onPress.bind(this)}>
                <Image source={{uri: this.props.news.urlToImage}} style={{width: '100%', height: 200}}/>
                <Text style={styles.titleStyle}>{this.props.news.title}</Text>
                <Text style={styles.timeStyle}>Published At: {new Date(this.props.news.publishedAt).toGMTString().replace('GMT', '')}</Text>
            </TouchableOpacity>
        )
    }

}

const styles = new StyleSheet.create({
    contentStyle: {
        padding: 10,
        margin: '3%',
        backgroundColor: 'white',
        marginBottom: '3%',
        borderRadius: 6,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

    titleStyle: {
        marginTop: 10,
        fontFamily: 'Nunito-SemiBold',
        fontSize: 16,
        color: 'black',
    },

    timeStyle: {
        marginTop: 10,
        marginBottom: 10,
        fontFamily: 'Nunito-Regular',
    }
})
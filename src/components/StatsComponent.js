import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default class StatsComponent extends React.Component {

    numberFormat = (num) => {
        if (typeof(num) === 'undefined') { return 'Loading ...' }
        return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,').replace(".00", "")
     }

    render() {
        return(
            <View style={styles.contentStyle}>
                <Text style={styles.titleStyle}>{this.props.title}</Text>
                <Text style={{...styles.coronaVirusTxt, color: this.props.color}}>{this.numberFormat(this.props.value)}</Text>
            </View>
        )
    }
}

const styles = new StyleSheet.create({
    contentStyle: {
        padding: 10,
        width: '48%',
        backgroundColor: 'white',
        height: 130,
        borderRadius: 6,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

    titleStyle: {
        fontFamily: 'Nunito-SemiBold',
        fontSize: 16,
        color: 'darkgray',
    },
    
    coronaVirusTxt: {
        marginTop: '5%',
        fontFamily: 'Nunito-Bold',
        fontSize: 30
      }
})
import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default class StatsList extends React.Component {

    numberFormat = (num) => {
        if (typeof(num) === 'undefined' || num === null) { return 'Loading ...' }
        return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,').replace(".00", "")
     }

    render() {
        return(
            <View style={styles.contentStyle}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={styles.titleStyle}>Deaths</Text>
                    <Text style={{...styles.titleStyle, fontSize: 20, color: 'rgb(255,59,48)'}}>{this.numberFormat(this.props.data.deceased)}</Text>
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={styles.titleStyle}>Recovered</Text>
                    <Text style={{...styles.titleStyle, fontSize: 20, color: 'rgb(52,199,89)'}}>{this.numberFormat(this.props.data.recovered)}</Text>
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={styles.titleStyle}>Infected</Text>
                    <Text style={{...styles.titleStyle, fontSize: 20, color: 'rgb(90,200,250)'}}>{this.numberFormat(this.props.data.infected)}</Text>
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={styles.titleStyle}>Tested</Text>
                    <Text style={{...styles.titleStyle, fontSize: 20, color: 'rgb(255,149,0)'}}>{this.numberFormat(this.props.data.tested)}</Text>
                </View>

                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text style={styles.titleStyle}>Date</Text>
                    <Text style={{...styles.titleStyle, fontSize: 13, color: 'black'}}>{new Date(this.props.data.lastUpdatedAtSource).toGMTString()}</Text>
                </View>

            </View>
        )
    }
}

const styles = new StyleSheet.create({
    contentStyle: {
        padding: 10,
        margin: '5%',
        backgroundColor: 'white',
        height: 155,
        marginBottom: '5%',
        borderRadius: 6,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

    titleStyle: {
        fontFamily: 'Nunito-SemiBold',
        fontSize: 16,
        color: 'black',
    }
})
import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { DrawerItemList } from '@react-navigation/drawer'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { firebase } from '@react-native-firebase/auth'

export default class DrawerHeader extends React.Component {

    constructor(props) {
        super(props)

        this.state = { name: '', email: '' }
    }

    componentDidMount() {
        const user = firebase.auth().currentUser
        if (user) {
            this.setState({ name: user.displayName, email: user.email })
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.contentStyle}>
                    <View style={styles.profileContentStyle}>
                        <Image source={require('../assets/profile.png')} style={{ tintColor: 'white', width: 48, height: 48 }} />
                        <View style={{ flexDirection: 'column', marginLeft: 20 }}>
                            <Text style={styles.nameStyle}>{this.state.name}</Text>
                            <Text style={styles.emailStyle}>{this.state.email}</Text>
                        </View>
                    </View>

                    <TouchableOpacity style={styles.logoutBtnStyle} onPress={() => firebase.auth().signOut()}>
                        <Text style={styles.logoutTextStyle}>Logout</Text>
                    </TouchableOpacity>
                </View>
                <DrawerItemList {...this.props} />
            </View>
        )
    }
}

const styles = new StyleSheet.create({

    contentStyle: {
        backgroundColor: '#6646ee',
        height: 150,
    },

    profileContentStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 40,
        marginLeft: 10
    },
    nameStyle: {
        fontFamily: 'Nunito-ExtraBold',
        fontSize: 20,
        color: 'white',
    },
    emailStyle: {
        fontFamily: 'Nunito-Bold',
        fontSize: 17,
        color: 'white',
    },
    logoutBtnStyle: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: 'white',
        height: 40,
        borderRadius: 10,
        justifyContent: 'center'
    },

    logoutTextStyle: {
        alignSelf: 'center',
        fontFamily: 'Nunito-Bold',
        fontSize: 20,
        color: 'black'
    }
})
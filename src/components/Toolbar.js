import React from 'react'
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native'

export default class Toolbar extends React.Component {
    render() {
        return(
            <View style={styles.contentStyle}>
                <TouchableOpacity onPress={this.props.onPress}>
                    <Image source={require('../assets/drawer.png')} style={{width: 30, height: 30, tintColor: 'white'}}/>
                </TouchableOpacity>
                <Text style={styles.titleStyle}>{this.props.title}</Text>
            </View>
        )
    }
}

const styles = new StyleSheet.create({

    contentStyle: {
        padding: 10,
        backgroundColor: '#6646ee',
        height: 55,
        shadowColor: "#000",
        marginBottom: '1%',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        alignItems: 'center',
        flexDirection: 'row'
    },

    titleStyle: {
        fontFamily: 'Nunito-SemiBold',
        fontSize: 21,
        color: 'white',
        marginLeft: 20
    }
})
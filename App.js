import React from 'react';
import Providers from './src/navigation';
import { DrawerNavigator } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';



export default function App() {
  return <Providers />;
}